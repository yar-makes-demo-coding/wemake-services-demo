defmodule Wemake.Repo do
  use Ecto.Repo,
    otp_app: :wemake,
    adapter: Ecto.Adapters.Postgres
end
