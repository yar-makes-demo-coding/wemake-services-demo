defmodule WemakeWeb.PageController do
  use WemakeWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
